ASM=nasm
ASMFLAGS=-f elf64
LD=ld
BUILD = ./build
TARGET = $(BUILD)/main

.PHONY: clean 


$(TARGET): $(BUILD)/main.o $(BUILD)/dict.o $(BUILD)/lib.o
	$(LD) -o $@ $^					 


$(BUILD)/%.o: %.asm
	mkdir -p $(BUILD)          
	$(ASM) $(ASMFLAGS) -o $@ $<


$(BUILD)/main.o: main.asm colon.inc words.inc lib.inc
	mkdir -p $(BUILD)          
	$(ASM) $(ASMFLAGS) -o $@ $<


clean:
	rm -rf $(BUILD)
