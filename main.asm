%define BUFFER_SIZE 255

%include "colon.inc"	
%include "words.inc"
%include "lib.inc"

extern find_word

section .bss
	buffer: times BUFFER_SIZE db 0
section .rodata
	error_message1: db "Too long", 0
	error_message2: db "Key is not exists", 0
		
section .text	

global _start

_start:
    xor rdi, rdi
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	cmp rax, 0
	je .bad1
	
	mov rdi, buffer
	mov rsi, NEXT
	push rdx
	call find_word
	pop rdx
	cmp rax, 0
	je .bad2

	lea rdi, [rdx+rax+9]
	
	call print_string
	xor rdi, rdi
	.end:
		push rdi
		call print_newline
		pop rdi
		call exit
	.bad1:
		mov rdi, error_message1
		call print_error
		mov rdi, 1
		jmp .end
	.bad2:                     
		mov rdi, error_message2
		call print_error     
		mov rdi, 2
		jmp .end
