%define NEXT 0

%macro colon 2
    %ifid %2
        %2: dq NEXT
        %define NEXT %2
    %else
        %error "Second value should be an id"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "First value should be a string"
    %endif
%endmacro
