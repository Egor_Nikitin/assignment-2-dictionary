global find_word

extern string_equals
	
find_word:
	add rsi, 8
	push rdi
	push rsi
	call string_equals
	pop rsi
	pop rdi
	sub rsi, 8
	
	cmp rax, 1
	jz .good

	mov rsi, [rsi]
	cmp rsi, 0
	jnz find_word

.bad:
	xor rax, rax
	ret

.good:
	mov rax, rsi
	ret
